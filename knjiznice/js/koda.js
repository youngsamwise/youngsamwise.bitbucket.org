/* global L, distance */

var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  var ehrId = "";

  // TODO: Potrebno implementirati
  var ime;
  var priimek;
  var datumRojstva;
  var drzava;
  var telVisina;
  var telTeza;
  var telTemperatura;
  var sisKrvniTlak;
  var DiaKrvniTlak;
  
  switch (stPacienta)
  {
    case 1:
      ime = "Carl";
      priimek = "Gallagher";
      datumRojstva = "2001-05-11";
      drzava = "ZDA";
      telVisina = "175";
      telTeza = "80";
      telTemperatura = "37.00";
      sisKrvniTlak = "120";
      DiaKrvniTlak = "70";
      break;
      
    case 2:
      ime = "Harry";
      priimek = "Potta";
      datumRojstva = "1990-11-05";
      drzava = "Narnia";
      telVisina = "201";
      telTeza = "130";
      telTemperatura = "36.00";
      sisKrvniTlak = "135";
      DiaKrvniTlak = "85";
      
      break;
      
    case 3:  
      ime = "Joseph";
      priimek = "Stalin";
      datumRojstva = "1878-11-18";
      drzava = "Gruzija";
      telVisina = "168";
      telTeza = "70";
      telTemperatura = "37.9";
      sisKrvniTlak = "162";
      DiaKrvniTlak = "102";
      
      break;
  }
  
  $.ajaxSetup(
    {
      headers: 
      {
        "Authorization": getAuthorization()
      }
      });
  $.ajax(
  {
    url: baseUrl + "/ehr",
    type: 'POST',
    success: function (data) 
    {
        var ehrId = data.ehrId;
        $("#header").html("EHR: " + ehrId);

         // build party data
        var partyData = 
        {
            firstNames: ime,
            lastNames: priimek,
            dateOfBirth: datumRojstva,
            partyAdditionalInfo: 
            [
              {
                key: "ehrId",
                value: ehrId
              }
            ]
        };
    $.ajax(
    {
      url: baseUrl + "/demographics/party",
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(partyData),
      success: function (party) 
      {
        if (party.action == 'CREATE') 
        {
          console.log("Dela");
        }
      }  
    });
  }
});


  return ehrId;
}
function generirajVse()
{
  generirajPodatke(1);
  generirajPodatke(2);
  generirajPodatke(3);
}

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

//MAPA
var mapa;
var obmocje;
const FRI_LAT = 46.05004;
const FRI_LNG = 14.46931;
var sharnjeniPoligoni = [];
window.addEventListener('load', function()
{
  var mapOptions = {
    center: [FRI_LAT, FRI_LNG],
    zoom: 12
    // maxZoom: 3
  };
  
  mapa = new L.map('mapa_id', mapOptions);
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
  mapa.addLayer(layer);
  console.log("Dodajanje uspešno");
  
  var obrnjeneKoordinate;
  
  // IZRIS VSEH BOLNIŠNIC 
  $.getJSON('https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json',function(json)
  {
    for(var i = 0; i< json.features.length;i++)
    {
      if(json.features[i].geometry.type == 'Polygon')
      {
        for(var j = 0; j < json.features[i].geometry.coordinates.length; j++)
        {
          for(var k = 0; k < json.features[i].geometry.coordinates[j].length; k++)
          {
            var a = json.features[i].geometry.coordinates[j][k][0];
            json.features[i].geometry.coordinates[j][k][0] = json.features[i].geometry.coordinates[j][k][1];
            json.features[i].geometry.coordinates[j][k][1] = a;
          }
        }
        var polygon = L.polygon(json.features[i].geometry.coordinates, {color:'blue'}).addTo(mapa);
        sharnjeniPoligoni.push(polygon);
      }
    }
    obrnjeneKoordinate = json;
  });
  
  mapa.on('click', bliznjeBolnisnice);
  
  function bliznjeBolnisnice(e)
  {
    var koordinate = e.latlng;
      // odstranimo prejšnje točke
      for(var i = 1; i< sharnjeniPoligoni.length;i++)
      {
        mapa.removeLayer(sharnjeniPoligoni[i]);
      }
    
      for(var i = 0; i< obrnjeneKoordinate.features.length;i++)
      {
        var features = obrnjeneKoordinate.features[i].properties;
        if(obrnjeneKoordinate.features[i].geometry.type == 'Polygon')
        {
          if(tockaVBlizini(koordinate,obrnjeneKoordinate.features[i].geometry.coordinates[0]))
          {
            var polygon = L.polygon(obrnjeneKoordinate.features[i].geometry.coordinates, {color:'green'}).bindPopup(features.name+"       "+features['addr:street'] + " " + features['addr:housenumber']).addTo(mapa);
          }
          else
          {
            var polygon = L.polygon(obrnjeneKoordinate.features[i].geometry.coordinates, {color:'blue'}).bindPopup(features.name+"       "+features['addr:street'] + " " + features['addr:housenumber']).addTo(mapa);
          }
        }
      }
  }
  
});

// ALI JE TOČKA V BLIZINI?
function tockaVBlizini(klik, tocka)
{
  if(distance(klik.lat,klik.lng,tocka[0][0],tocka[0][1], "K") > 10)
  {
    return false;
  }
  return true;
}

